import { characters } from "app/bartholomew-faire";

export const OUTLINE = require('raw-loader!./outline.md');

export const PLAY = {
  characters: {
    littlewit: { name: 'Littlewit', description: 'character description' },
    winthefight: { name: 'Win-the-Fight', description: 'character description' },
    winwife: { name: 'Winwife', description: 'character description' },
    quarlous: { name: 'Quarlous', description: 'character description' },
    wasp: { name: 'Wasp', description: 'character description' },
    cokes: { name: 'Cokes', description: 'character description' },
    grace: { name: 'Grace', description: 'character description' },
    mistressoverdo: { name: 'Dame Overdo', description: 'character description' },
    solomon: { name: 'Solomon', description: 'character description' },
    damepurecraft: { name: 'Dame Purecraft', description: 'character description' },
    zealoftheland: { name: 'Zeal-of-the-Land', description: 'character description' },
    busy: { name: 'Busy', description: 'character description' },
    overdo: { name: 'Justice Overdo', description: 'character description' },
    leatherhead: { name: 'Leatherhead', description: 'character description' },
    trash: { name: 'Trash', description: 'character description' },
    ursula: { name: 'Ursula', description: 'character description' },
    mooncalf: { name: 'Mooncalf', description: 'character description' },
    costermonger: { name: 'Costermonger', description: 'character description' },
    knockem: { name: 'Knockem', description: 'character description' },
    edgeworth: { name: 'Edgeworth', description: 'character description' },
    nightingale: { name: 'Nightingale', description: 'character description' },
    corncutter: { name: 'Corncutter', description: 'character description' },
    tinderboxman: { name: 'Tinderbox Man', description: 'character description' },
    passengers: { name: 'Passengers', description: 'character description' },
    whit: { name: 'Capt Whit', description: 'character description' },
    haggis: { name: 'Haggis', description: 'character description' },
    bristle: { name: 'Bristle', description: 'character description' },
    troubleall: { name: 'Trouble All', description: 'character description' },
    mistresslittlewit: { name: 'Mistress Littlewit', description: 'character description' },
    poacher: { name: 'Poacher', description: 'character description' },
    nordern: { name: 'Nordern', description: 'character description' },
    puppy: { name: 'Puppy', description: 'character description' },
    cutting: { name: 'Cutting', description: 'character description' },
    alice: { name: 'Alice', description: 'character description' },
    filcher: { name: 'Filcher', description: 'character description' },
    sharkwell: { name: 'Sharkwell', description: 'character description' },
    puppetleander: { name: 'Puppet Leander', description: 'character description' },
    puppetcole: { name: 'Puppet Cole', description: 'character description' },
    puppetjonas: { name: 'Puppet Jonas', description: 'character description' },
    puppetdamon: { name: 'Puppet Damon', description: 'character description' },
    puppetpythias: { name: 'Puppet Pythias', description: 'character description' }
  }
}
