# Sejanus Outline

## Act 1
The play begins in a stateroom in a place with Sabinus and Silius expressing their disappointment in Tiberius and the influence Sejanus has over him. They believe Rome can benefit from the rule of Drusus in whom they have hope because he does not support Sejanus
and has influence on his father, Tiberius.

Sabinus and Silius are joined by Sejanus’ followers (spies) Natta and Latiaris. Sabinus and Silius criticize them for their blind following of and belief in Sejanus, as they try to act the way he does and be around him at all times.

Cordus and Arruntius join them, as Natta and Latiaris whisper about who Cordus supports because he is a record keeper of history and they are concerned about their place in history. Arruntius talks about how times have changed and the bravery of men, as there are no more brave men. Drusus passes by, and they acknowledge him as a nobleman because of his
support of Germanicus’ family and that the old virtues of Romans are still in him. They discuss Germanicus’s great rule and his death by Tiberius’s planning, plotting, and false accusations of treason and poison. Silius acknowledges that Sejanus will carry out these actions as well to get his way.

As Sejanus enters, Natta and Latiaris stop talking about him and begin discussing Livia’s physician, Eudemus. Sejanus wants to talk in private with the physician and orders he is brought to him. This conversation happens as these characters are walking across the stage. Upon their
exit, Arruntius talks with Cordus about his fears of Sejanus’s plotting and the fact that his power and army are increasing, and he is almost equal to Tiberius. They are fearful of his power and what he could choose to do with it.

Satrius, a client of Sejanus, enters with Edemus and informs him prior to Sejanus’s entry that he needs to behave himself and show extra respect to Sejanus as he is the noblest Roman. Sejanus enters and shakes hands with Eudemus and tells him that he has an illness that requires
his medical help. After introductions, Satrius leaves. Sejanus begins asking about the ladies of the court, in particular, Livia, Drusus’s wife, which makes Eudemus uncomfortable, but Sejanus assures him that he is not interested in their bodies or how they look. He confesses his love for Livia. Once Sejanus had entrusted Eudemus with his secret and asks him to inform Livia of his love for her, Eudemus leaves and Sejanus reflects about his plot to kill Drusus using the beautiful Livia to fulfill his plans.


Tiberius, Drusus, and Haterius enter and someone kneels before Tiberius to which he refuses stating that humans only kneel to gods, not other humans. Sejanus takes this opportunity to compare Tiberius to a god, which Tiberius readily accepts without argument. Haterius and
Laterius present letters from the senate to Tiberius, which are invitations to come to the senate. Silius gives a speech about Tiberius, listing his lack of control of and goodwill to Rome. Arruntius expresses his desire to kill Sejanus and his followers, as he sees them as rats infesting the castle and Rome. He wishes to inform Tiberius of the influence and plotting of Sejanus. Silius advises against that and Arruntius prays to be saved from a tyrant, referring to Tiberius, or
worse a flatterer, referring to Sejanus. Tiberius speaks to the senate about Spain wanting to make a statue in his likeness, but he refuses because he views this as a comparison to a god and he sees
himself as merely a man. He continues to say the statue would be flattering, but he believes flattery ruin leaders, for which he is praised by Natta, Saterius, and Sejanus. Arruntius states he
wants the talk to end as he believes Tiberius is lying.


Tiberius continues and informs the senate of the statue of Sejanus in Pompey’s theatre for his brave rescuing of the theatre from a fire; he praises Sejanus for his merit and the help he offers. Cordus states Sejanus’s statue would ruin Pompey’s theatre and Arruntius agrees. Drusus
is in disbelief of what his father has done to make Sejanus equal to him with these honors and titles and prays Sejanus will be content with his allotment thus far and will not need or want more honors and titles. Sejanus enters with followers and ignores Drusus’s presence and Drusus
confronts his dismissal and strikes him. Arruntius praises Drusus for this action. Drusus draws his sword toward Sejanus and warns him to be fearful, as he will make a statue of him on a cross. Sejanus is left alone, and he vows to take revenge on Drusus for the blow and begins putting his plot in motion.

Questions:
1. How does Jonson’s use of theatrical performance affect our understanding of the political
or social unrest within the play?
2. What is the importance of having Sejanus’s spies known to the audience but not to others
on the stage?
3. How do Sibanius and Arruntius’ side conversations alter our understanding of the
plotline?
4. Why does Tiberius refuse a statue of himself in Spain but implores there be one erected
for Sejanus?


## Act 2

Sejanus meets Livia with the aim to seduce her through the help of Eudemus. The three
of them agree on a plan to poison Drusus Senior. Livia suggests that they could manipulate
Lygdus, Drusus Senior’s eunuch, into giving the poison to Drusus Senior. Sejanus opts to convince Lygdus to assist them. Following this Sejanus begins giving Livia a speech, justifying
their motives for poisoning Drusus Senior, during the speech he declares that adultery should be
the least of their concerns for it is the least of the crimes they will be committing. He projects
their future together as rulers of Rome. Sejanus then leaves to attend to the door, allowing
Eudemus to persuade Livia of Sejanus’s greatness and to discover Livia’s feelings towards
Sejanus. Sejanus re-enters to give Livia a melodramatic speech asking that she not fall back in
love with Drusus Senior. He gives Livia many apologies for his manners of having to leave such
an excellent lady so abruptly. Livia and Eudemus clarify that they both have the same feelings
towards Drusus Senior and ensure their both following through with the plan. 


Sejanus goes to meet with Tiberius to help ease his concerns about Agrippina, the widow of
Germanicus, and her supporters. Sejanus reveals to the audience his internal thoughts and
mischievous motives towards Tiberius. Tiberius confides in Sejanus with his worries about his
secret murder of Germanicus. Tiberius fears that the public will discover this, and the citizens
will revolt against him and overthrow him, leaving the throne to Agrippina. Sejanus plays on
Tiberius’ fear and proposes a plan that requires Tiberius to win the trust of the Agrippina
supporters by providing a sense of security.


Tiberius proposes imprisonment of the alleged traitors, but Sejanus suggests that the
group is already too well organized in their schemes. He advises the emperor not to show his
suspicions for fear that the band would begin their plot early. Instead, Sejanus proposes the
emperor grant them further favour in the courts to pacify them, while simultaneously dismantling
the image of its key members behind the scenes. Tiberius initially declines to have any of the
group put to death, but is immediately convinced by Sejanus. The pair decides that the first of
their targets will be Silius as, through his history as an imperial commander, he poses the largest

threat. Sejanus requests Tiberius call a meeting of the senate, where he will prove Silius’ guilt.
The emperor requests that they discuss the plan, but Sejanus convinces him that they will “thrive
more by execution than advice”.


Posthumus, a spy of Sejanus, enters to discuss his findings on the group secluded in
private at Agrippina’s house. The spy reports very little, as the members were speaking in code
and avoiding public discussion, which Sejanus interprets as an affirmation of their guilt. Without
any new knowledge of the group’s activities, Sejanus sends the spy back to embellish stories of
Silius’s treason to the nobility and Agrippina herself, whom he believes he can convince. Now
alone, Sejanus begins a soliloquy on his intentions to manipulate Tiberius into becoming a tyrant.
His plan is to turn the emperor’s citizens against him before attempting to steal the throne.


Satrius and Natta, other spies of Sejanus, are seen around the Agrippina house, hiding
quickly as the group draws near. Arruntius, Cordus, and Sabinus remark how Sejanus’ men have
been baiting the group into speaking treasonously. Silius leaves his wife Sosia with Agrippina,
saying that she can discourse with Sejanus’s men should they try to slander Agrippina. Agrippina
refuses, stating that she refuses to whisper her loyalties. Reaffirming the danger, Silius reminds
her that Sejanus and Tiberius were responsible for the death of her husband Germanicus.
Drusus Junior enters the house, stating that Drusus Senior is dying. Suspicious of the
sudden nature of his illness and involvement of Eudemus, Silius immediately suspects Drusus
Senior has been poisoned. He recounts the exchange where Drusus Senior struck Sejanus as well
as the secret meetings with Livia, which leads him to believe that Sejanus is against everyone for
his own ambitions. Drusus Junior informs Silius that he is due in court, and he departs.


Questions:

1. What is implied by or important about the fact that Lygdus is Drusus’s cup bearer?
2. How do entrances and exits at the outset of act two assist Sejanus in his plot to manipulate
Livia?
3. How might Sejanus’ oversights when spying on the Agrippina group be indicative and
symptomatic of his oversight to Tiberius’ suspicions?
4. In what ways does Jonson use the concept of rumour to affect the audience? What rumours,
if any, is the audience supposed to heed as truthful?

## Act 3

The senate convenes in the wake of Drusus’s poisoning, lamenting that Tiberius will not
be in attendance following the death of his heir. Gallus questions the purpose of the senate’s
meeting without Tiberius’s presence, to which Arruntius makes a snide comment. Tiberius
enters, to the surprise of all, wasting no time in beginning an address to the senate and bringing
his adopted sons, Drusus Junior and Nero, before the senate. He feigns giving them his blessing
as true sons of Rome and worthy successors of his throne (Arruntius and the senators are
unconvinced and remaining suspicious throughout). Sejanus intercuts this speech with false
expressions of solidarity for Tiberius’s rule, which is made immediately suspect to the senate. 


Following Tiberius’s address, court proceedings resume. Varro accuses Silius of being a
war criminal and traitor to Rome for the drawn-out war he waged in Gallia. Silius calls Tiberius
to his aid but to no avail, as Arruntius laments that he is being charged under proper due process.


Afer notes that Silius conquered in the name of Rome and remained loyal to Tiberius
even following mutinies and rebellions. Silius begins a speech in which he asserts that Tiberius’s
rule is one built on slavery not action, and that he knows violence rather than leadership. Sejanus

takes this outburst as proof that Silius is Rome’s enemy, but before judgement can be passed on
him, Silius dies by suicide.


The court reacts to Silius’s suicide. Arruntius believes this act was noble while Tiberius
views it as unnecessary because he claims he only meant to take his state away. Sejanus, upon
hearing Tiberius’s reaction, declares that it is just for Silius to die, as he is a traitor. This quickly
turns into an argument over how Silius’s wealth should be divided.

 
Cordus is then brought in and accused by two of Sejanus’s followers, Satrius and Natta.
He is accused of praising Brutus and Cassius in the annals he wrote. To defend himself, he does
not deny that he honoured them in his writing, but he goes on to list a number of people who also
praised those considered threats to the state. He elucidates that they were treated fairly and not
accused as he is. Tiberius decides to put the case off for a while but will burn Cordus’s books in
the meantime. Arruntius is upset by the decision to burn the books, but Sabinus points out that
when books are burned, the author will likely become well-known.


After the court scene, Tiberius and Sejanus enter a room alone and discuss the matter.
Tiberius believes the proceedings went well in achieving the goal of preventing the people from
scheming against Tiberius. Sejanus suggests they accuse Sabinus next, while Tiberius wishes to
accuse Arruntius first. Sejanus believes Arruntius is still a useful asset to them, however, as he is
talkative and bound to share secrets with them. Sejanus then proceeds to request that Tiberius
give him Livia’s hand in marriage, but Tiberius says it is up to her to decide and that she would
not want him for a husband because her last two husbands held a higher status than him. When
Tiberius leaves, it is made clear that Sejanus is upset with this decision and plans to take over
Tiberius’s position if he should leave Rome, which he suggested Tiberius should do. 

Tiberius enters again, after Sejanus leaves, to state he suspects that Sejanus is plotting
against him because he has been asking for too many favors. With his upcoming leave of Rome
making his state vulnerable to those trying to overthrow him, Tiberius summons Macro to spy on
Sejanus and govern while he is gone. When Tiberius exits, Macro states that he will not question
Caesar’s actions, but do whatever he asks. Macro believes if Sejanus is removed from power, he
will rise to power himself.


Questions:

1. In act one, Arruntius is eager to act and inform Tiberius that he is among deceivers, and he is
told to stay and wait for the right moment by Sabinus. How does this resemble the court
proceedings of act three wherein Varro tells Silius to have patience while he lists his charges?
2. Tiberius’s address to the senate in act three is clearly hypocritical and deceitful, given
Arruntius’s asides. Who, then, did Tiberius most likely intend to deceive with his speech? Why?
3. Tiberius decides to burn Cordus’s books because they speak well of past Roman leaders who
were not esteemed, which he views as disrespectful toward his leadership. Thinking of other
leaders in history who have burned books; what does the erasure of history accomplish?
4. When Tiberius suggests he must take caution not to praise any man too much, as he did with
Sejanus, he immediately starts praising Macro as his faithful servant and places his trust in him.
What does this foreshadow about Macro and say about Tiberius as an emperor?

## Act 4

The scene begins at Agrippina’s residence with a discussion between Agrippina and
Gallus. In this discussion, Agrippina voices her utmost displeasure with the happenings of the
Roman political system. Gallus, who seems to claim that her safety might be best preserved if
she stays quiet on the matters, is cut short by Agrippina, who bemoans the situation she has found herself in between Sejanus and Tiberius’s allegiances. She tells Gallus to leave, as she
further criticizes the success Sejanus has had in manipulation.
 
She calls for her sons Nero, Drusus Junior, and Caligula, who enter with bad news. Nero
tells his mother the story of how Tiberius became trapped inside a cave while in a place called
Spelunca. When soldiers came to rescue Tiberius, they found that all of Tiberius’s allies had
either fled or were unwilling to help. Sejanus was the only one who was trying the save Tiberius,
and as a result of this loyal action, Sejanus has returned from his visit with Tiberius with more
praise than ever. In hearing this story, Agrippina warns her sons of their vulnerability, but also
tries to instill confidence in them for the impending conflicts to which they likely be subjected. 


Macro, who is in the streets, gives an aside in which he questions the motives and
intelligence of Tiberius, who had just previously sent him on a mission to spy on Sejanus and his
followers. Macro questions whether Tiberius is being intentionally naive, or if he is truly
unaware of the threat Sejanus poses to his power. 


Back at Agrippina’s residence, Rufus and Opsius hide in the attic and wait for Latiaris to
lure Sabinus closer to them. Latiaris enters with Sabinus, and proceeds to discuss how noble
Sabinus has been to Tiberius despite the recent hardships they have faced. Latiaris continues to
flatter Sabinus, and in doing so leads Sabinus into accidently slandering Tiberius. Upon hearing
these disparaging words come from Sabinus, Opsius and Rufus emerge from their hiding spots,
accusing Sabinus of treason. They take him prisoner and exit.


Macro and Caligula meet in the street, and Macro warns Caligula of the dangers he is
facing. He talks of Caligula’s mother and brother being called to the senate and how Sejanus’
followers have already captured and imprisoned Sabinus. When Caligula asks for advice, Macro states that he should seek refuge by staying with his Uncle Tiberius. Macro claims Caligula will
be much safer from Sejanus’s plots if he is under direct surveillance of his uncle. They both
agree on this plan, and Caligula sets off on his journey.

 
Arruntius wonders aloud why Tiberius has allowed Sejanus to garner so much power and
trust. He questions how much open scheming and plotting it will take from Sejanus before
Tiberius begins to realize his true intentions.


Lepidus enters the scene and Arruntius addresses him by claiming that they are among
the few honest few people left that have not allied with Sejanus. Arruntius further asks him to
give him advice concerning political survival, noting Lepidus’s age as a sign that he must be
skilled at it. Lepidus replies that the only art in survival is to remain as passive as possible so as
to not put oneself in danger. Arruntius expresses his paranoia at being seen as a traitor as well as
his distaste for the current dangerous political climate.


Laco, Nero, and Lictors enter. Prince Nero is escorted as a prisoner by Laco. Prince Nero
tells Arruntius and Lepidus to be careful what they say so that they don’t meet the same fate he
has. Lepidus asks Laco where Prince Nero is going, and Laco tells him that he’s been banished
to Pontia by the senate, and that his brother Drusus Junior is being held prisoner in Caesar’s
palace and that Agrippina is confined in Pandataria, for they have all been charged with treason.
Lepidus describes how people before would not object to the claims that the Prince was being
conspired against by Sejanus, and now that royal figures are being imprisoned and exiled, no one
dares speak up, as Sejanus has crushed the dissident’s voices.


Arruntius then tells Lepidus that he does not believe Tiberius. The Prince is conscious of
the conspiratorial forces that congregate in his house, or if he understands that Sejanus is a
traitor, then he is acting purposefully naive. Arruntius then describes what is wrong with Sejanus: he is an unfit ruler, he is living on an obscure island, and he is excessively cruel. Laco
re-enters with Pomponius and Minutius. Arruntius overhears them discussing whether or not
Tiberius will come back to Rome. They also overhear Laco discussing how difficult it is to
understand who has Sejanus’s favour, to which Pomponius replies that they should seek to be
viewed favourably, as Sejanus is rapidly rising in political power. Minutius expresses doubt, as
Regulus, an enemy of Sejanus, was advanced in consulship.


Lepidus describes his belief that Tiberius must be conscious of Sejanus’s treachery and
attempts at usurpation, Arruntius replies by suggesting that Tiberius, an experienced political
tyrant, may be feigning ignorance in order to persuade Sejanus to have a false sense of security
before he comes home to execute him. Terentius then presents Tiberius’s letters to Pomponius,
Laco, and Minutius. The four of them examine the letters, and they come to the conclusion that
Tiberius does not suspect Sejanus, as he still refers to him respectfully and endows him with
money, but they nevertheless express doubts. Arruntius and Lepidus exit, and Pomponius voices
his belief that Tiberius will not attempt revenge against Sejanus in court, as Sejanus has now
gained popular favour. The scene ends with Laco asking why Macro trusts Caligula. Pomponius
replies that Macro is easily fooled and persuaded. The two men depart in good will.


Questions:
1. If Opsius, Rufus, and Latiaris are Sejanus’s allies, why do they accuse Sabinus of treason to
Tiberius? What does this say about the legitimacy of Tiberius as a ruler?
2. Macro questions the intelligence and motives of Tiberius. Keeping this in mind, why does he
still believe Caligula is safest if he stays with his uncle Tiberius?
3. When Arruntius expresses concern over accidentally expressing bad will against Tiberius and
Sejanus, why does he express worry about bad will against Tiberius if he is allied with him?

4. What are Tiberius’s true intentions behind omitting Sejanus’s rank and titles in the letter that
he wrote to him?

## Act 5

Sejanus declares his superiority through a soliloquy; he believes he is destined for
greatness because his power is greater than the gods’ in the heavens. Terentius then warns
Sejanus to guard himself, as his statue in Pompey’s theatre is expelling black smoke. Sejanus
informs Terentius to go remove the head and unfold the message within it. When the head was
removed, we are informed that a giant snake slithered out. Sejanus does not believe in bad
omens, but he decides to pray to the gods for a change in fortune.


Macro and Regulus discuss a plan before Laco delivers news that Caesar is well. They
decide to gather the council to meet at the senate in the morning to discuss the demise of
Sejanus. The Praecones, the Flamen, and the Ministri pray to the heavens for a change in
Sejanus’s fortune through a service of offerings in Sejanus’s house. The gods turn away from the
offerings which does not frighten Sejanus until news returns that the head of his statue has been
replaced by a fiery meteor. Sejanus demands more guards for protection. He is confused about
his fortune and describes all of the great warriors he has conquered to reassure himself of his
superiority.


Sejanus is informed of the gathering at the senate and that Macro would like to meet
privately before attending. Sejanus has Laco guard the door outside of the room for extra
precaution. Macro flatters Sejanus excessively before informing him that the council wishes to
crown him for his service and to fear nothing of the bad omens. Sejanus informs his followers
that there is nothing to fear as he will finally be crowned as he truly deserves.


Arruntius and Lepidus are surprised with the news from Sejanus and believe something
seems to be suspicious after all of the bad omens Sejanus had received. Macro asks Laco to lock
the temple doors and protect it with guards once Sejanus enters and then only enter when hearing
commotion and take down any man that attacks.


The senate is complimenting Sejanus in order to keep their own positions, given their
anticipation that Sejanus will be promoted. A letter from Tiberius concerning Sejanus is then
read to all the people at the senate. This letter instead recommends stripping Sejanus of all his
power and titles because of his appalling actions. Macro then has Sejanus and his followers,
Latiaris and Natta, arrested and removed from the senate. All the people who were flattering
Sejanus are now condemning him. Lepidus and Arruntius discuss how popularity can disappear
so quickly. Marco is praised by the senate. Arruntius sees how the senators’ praise will cause
Macro to become greater than Sejanus.


Terentius explains to Lepidus and Arruntius how a multitude of people hear of Sejanus’s
fall. The multitude tear down Sejanus’s statue and drag it through the streets. Sejanus was
sentenced by the Senate to die by beheading but instead was captured by the same multitude and
torn to pieces.


Nuntius tells Arruntius, Lepidus, and Terentius how Sejanus’s son and daughter were
captured. Roman law does not allow virgins to die, so Macro coordinates her rape with the
hangman and then both the daughter and son are killed and presented publicly. Their mother
finds them, is devastated, and vows to prove Livia, Lygdus, and Eudemus guilty of poisoning
Drusus Senior.


The multitude are now unhappy with the way they handled things. Some who tore
Sejanus apart wish they could reverse what happened. Terentius finishes with a remark about how an insolent and proud man who does not care for the gods can, in a moment, fall in the
manner Sejanus has.

Questions:

1. How does Sejanus’s speech at the opening of act five engage directly with the audience?
2. What is the significance of the elaborate prayer at Sejanus’s home and its ineffectual nature?
3. In what way does Arruntius mean when he observes that the praise from the Senate will
make Macro greater than Sejanus?
4. During the conclusion there is a quick succession of events that take place, what is the main
point Ben Jonson is trying to convey though the speedy changes?

## General Questions

#### Given Jonson’s notorious penchant for dense intertextuality and editorial work, and his seeming disdain for the impermanence of theatre, what drew him to work in theater, rather than write literature?
Burrows, Ian Roger. “‘[​Overhearing​]’: Printing Parentheses and Reading Power in Ben Jonson’s Sejanus​.” ​Early Theatre,​ vol. 20, no. 2, 2017, pp. 99–119, doi:10.12745/et.20.2.2811.


#### Would changing historical events or characters in order to make them more relatable the early modern audience change the message or importance of the event or character?
Byrne, Peter. “‘The Cunning of Their Ground’: The Relevance of Sejanus to Renaissance Tragedy.” Early Theatre, vol. 17, no. 1, 2014, pp. 113–135.

#### Sejanus, His Fall was not well received by audiences when Jonson wrote it. How does this parallel with the trial of Cordus and his historical annals?
Chalk, Brian Patrick. “Jonson&#39;s Textual Monument.” Studies in English Literature 1500-1900, vol. 52 no. 2, 2012, pp. 387-405, doi:10.1353/sel.2012.0014.


#### What does Sejanus say about male friendship in relation to power?
Christopher, Brandon. “‘Associate of our labours’: Ben Jonson’s Sejanus and the Limits of Master-Secretary Friendship.” Ben Jonson Journal, vol. 19, no. 1, 2012, pp. 105-126.


#### David Farley-Hills notes a number of authors and playwrights that Ben Jonson likely studied and tried to imitate. Who are some other authors or playwrights not mentioned in this article that Jonson may have studied or tried to emulate in his works?
Farley-Hills, David. “Jonson and the neo-Classical rules in Sejanus and Volpone.” The Review of English Studies, vol. 46, no. 182, May 1995, pp. 153-73.

#### What similarities are there in depictions of gender and politics across Jonson’s Roman dramatic tragedies?
Gaggero, Christopher. “Civic Humanism and Gender Politics in Jonson’s Catiline.” Studies in English Literature, 1500-1900, vol. 45, no. 2, Spring 2005, pp. 401-24.


#### Does Jonson’s focus on or discussion of the annals change the validity of the play? Consider if their inclusion questions Jonson’s factual history.
Gibbard, Peter. “Ben Jonson&#39;s Sejanus and the Middle Way of Annals 1-6.” Studies in English Literature, vol. 56, no. 2, 2016, pp. 307-25.



#### Is Ben Jonson’s Sejanus a historical account, a contemporary adaptation of his time, or both? If both, what is the primary contributor to the overall plot of Sejanus’s rise and fall?
Goldfarb, Philip. “Jonson’s Renaissance Romans: Classical Adaptation in Sejanus.” Interdisciplinary Humanities, vol. 21, no. 3, Fall 2014, pp. 53-62.



#### Kurland proposes that in the tumultuous political climate of the Rome depicted in Sejanus,political corruption overrides that of lineage or kin. What are some recent (within the last two hundred years) examples of this?
Kurland, Stuart M. “‘No innocence is safe, When power contests’: The Factional Worlds of Caesar and Sejanus.” Comparative Drama, vol. 22, no. 1, 1988, pp. 56-67, doi:10.1353/cdr.1988.0002.


#### How does Lenthe illuminate that Jonson’s Catholicism is a political act that has an effect on his writing?
Lenthe, Victor. “Ben Jonson&#39;s Antagonistic Style, Public Opinion, and Sejanus.” Studies in English Literature 1500-1900, vol. 57, no. 2, 2017, pp. 349-68.
