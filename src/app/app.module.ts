import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { ClarityModule } from '@clr/angular';
import { MarkdownModule } from 'ngx-md';
import * as BartholomewFair from 'plays/bartholomew-fair';
import * as Sejanus from 'plays/sejanus';

import { AppComponent } from './app.component';
import { CharacterLabelComponent } from './components/character-label/character-label.component';
import { PlayComponent } from './components/play/play.component';
import { SceneComponent } from './components/scene/scene.component';
import { ScrollSpy } from './components/scrollspy.directive';
import { SectionComponent } from './components/section/section.component';
import { D3_DIRECTIVES, D3Service } from './d3';
import { PlayService } from './services/play.service';
import { GraphComponent } from './visuals/graph/graph.component';
import { SHARED_VISUALS } from './visuals/shared';

const getWindow = () => window;


const ROUTES = [
  {
    path: 'sejanus',
    component: PlayComponent,
    data: {
      play: Sejanus.PLAY,
      outline: Sejanus.OUTLINE,
    },
  },
  {
    path: 'bartholomew-fair',
    component: PlayComponent,
    data: {
      play: BartholomewFair.PLAY,
      outline: BartholomewFair.OUTLINE,
    },
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'bartholomew-fair',
  }
];
@NgModule({
  declarations: [
    AppComponent,
    GraphComponent,
    ...SHARED_VISUALS,
    ...D3_DIRECTIVES,
    PlayComponent,
    ScrollSpy,
    CharacterLabelComponent,
    SceneComponent,
    SectionComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(ROUTES),
    FormsModule,
    MarkdownModule.forRoot(),
    HttpModule,
    ClarityModule,
  ],
  providers: [D3Service, PlayService],
  bootstrap: [AppComponent],
})
export class AppModule {}
