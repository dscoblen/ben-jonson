import { Component, OnInit, HostBinding, ChangeDetectorRef } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Node, Link } from '../../d3';
import { PlayService, Section } from 'app/services/play.service';

interface Nav {
  section: Section;
  children: Section[];
}
@Component({
  selector: 'app-play',
  templateUrl: 'play.component.html',
  styleUrls: ['./play.component.scss']
})
export class PlayComponent implements OnInit {
  @HostBinding('class.content-container') contentArea = true;
  nodes: Node[] = [];
  links: Link[] = [];
  play = undefined;
  outline = undefined;
  sections: Section[] = [];
  nav: Nav[] = [];


  constructor(private route: ActivatedRoute, private router: Router, private playService: PlayService, private cdr: ChangeDetectorRef) {
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const tree = router.parseUrl(router.url);
        if (tree.fragment) {
          const element = document.querySelector('#' + tree.fragment);
          if (element) { element.scrollIntoView(true); }
        }
      }
    });
  }

  buildGraph() {
    const nodeMap: any = {};
    for (const prop in this.play.characters) {
      if (this.play.characters[prop]) {
        const node: Node = new Node(prop);
        this.nodes.push(node);
        nodeMap[prop] = node;
      }
    }
    // this.play.acts.forEach(act => {
    //   act.scenes.forEach(scene => {
    //     const people = scene.characters;

    //     for (let i = 0; i < people.length - 1; i++) {
    //       for (let j = i + 1; j < people.length; j++) {
    //         const n1 = nodeMap[people[i]];
    //         const n2 = nodeMap[people[j]];
    //         const id = n1.id + '_' + n2.id;
    //         const idReverse = n2.id + '_' + n1.id;
    //         n1.linkCount = n2.linkCount + 1;
    //         n2.linkCount = n2.linkCount + 1;
    //         const existing = this.links.find(link => (link.title === id) || (link.title === idReverse));
    //         if (existing) {
    //           existing.count++;
    //         } else {
    //           this.links.push(new Link(n1.id, n2.id, id));
    //         }
    //       }
    //     }

    //   });

    // });

  }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.play = data.play;
      this.outline = data.outline;
      this.playService.parsePlay(this.play, this.outline);
      this.sections = this.playService.sections;

      let currentNav = undefined;
      this.sections.forEach(section => {
        if (section.level === 2) {
          currentNav = { section: section, children: [] }
          this.nav.push(currentNav);
        } else if (section.level === 3 && currentNav) {
          currentNav.children.push(section);
        }
      });
      this.buildGraph();
      this.cdr.detectChanges();
    });
  }

}
